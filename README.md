# Structure factor

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/eivindbering%2Fstructure-factor/HEAD?labpath=structure_factor_from_file2.ipynb) 

Jupyter notebook to get the structure factor from MD-trajectory-files by 3D FFT. It can be run from the binder link above, but it might be a bit slow to start up.

The script exists in two versions, structure_factor_from_file.ipynb and structure_factor_from_file2.ipynb, the difference is the choice of visualization, the latter appears to work best for me. Both script have helper functions in the scripts helper_functions.ipynb and helper_functions2.ipynb respectively. 

The notebook setup.ipynb can be run to install dependencies.